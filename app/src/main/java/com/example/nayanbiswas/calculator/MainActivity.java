package com.example.nayanbiswas.calculator;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    String result = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void runHandler(View view) {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        TextView tvResult = ((TextView) findViewById(R.id.tvResult));
        Calculator equ = new Calculator();
        String input = tvInput.getText().toString();
        try {
            if(input.length()>0) {
                result = Double.toString(Simplify.round(equ.result(input), 6));
                tvResult.setText(result);
            }
        } catch (Exception e) {
            tvResult.setText(Simplify.findErrorMessage(e.toString()));
        }
    }

    private void clear() {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        TextView tvResult = ((TextView) findViewById(R.id.tvResult));

        tvInput.setText("");
        tvResult.setText("");
    }

    public void AllClearHandler(View view) {
        clear();
    }

    public void historyHandler(View view) {
    }

    public void deleteHandler(View view) {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        String str = tvInput.getText().toString();
        if (str.length() == 0) return;
        str = str.substring(0, str.length() - 1);
        tvInput.setText(str);
    }

    public void zeroHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "0"));
    }

    public void oneHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "1"));
    }

    public void twoHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "2"));
    }

    public void threeHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "3"));
    }

    public void fourHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "4"));
    }

    public void fiveHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "5"));
    }

    public void sixHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "6"));
    }

    public void sevenHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "7"));
    }

    public void eightHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "8"));
    }

    public void nineHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "9"));
    }

    public void dotHandler(View view) {
        if (result.length() > 0) {
            clear();
            result = "";
        }
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        String input = tvInput.getText().toString();
        if(Simplify.validDot(input)) {
            tvInput.setText(Simplify.setBtnInput(input, "."));
        }
    }

    public void plusHandler(View view) {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        String str = tvInput.getText().toString();
        if (result.length() > 0) {
            clear();
            str = result + tvInput.getText().toString();
            result = "";
        }
        tvInput.setText(Simplify.setBtnInput(tvInput.getText().toString(), "+"));
    }

    public void minusHandler(View view) {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        String str = tvInput.getText().toString();
        if (result.length() > 0) {
            clear();
            str = result + tvInput.getText().toString();
            result = "";
        }
        tvInput.setText(Simplify.setBtnInput(str, "-"));
    }

    public void multiplyHandler(View view) {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        String str = tvInput.getText().toString();
        if (result.length() > 0) {
            clear();
            str = result + tvInput.getText().toString();
            result = "";
        }
        tvInput.setText(Simplify.setBtnInput(str, "*"));
    }

    public void divisionHandler(View view) {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        String str = tvInput.getText().toString();
        if (result.length() > 0) {
            clear();
            str = result + tvInput.getText().toString();
            result = "";
        }
        tvInput.setText(Simplify.setBtnInput(str, "/"));
    }

    public void modulusHandler(View view) {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        String str = tvInput.getText().toString();
        if (result.length() > 0) {
            clear();
            str = result + tvInput.getText().toString();
            result = "";
        }
        tvInput.setText(Simplify.setBtnInput(str, "%"));
    }

    public void parenthesesHandler(View view) {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        String str = tvInput.getText().toString();
        if (result.length() > 0) {
            clear();
            str = result + tvInput.getText().toString();
            result = "";
        }
        tvInput.setText(str+Simplify.validParentheses(str));
    }

    public void plusMinusHandler(View view) {
        TextView tvInput = ((TextView) findViewById(R.id.tvInput));
        String str = tvInput.getText().toString();
        if (result.length() > 0) {
            clear();
            str = result + tvInput.getText().toString();
            result = "";
        }
        tvInput.setText(Simplify.setBtnInput(str, "!"));
    }


}