package com.example.nayanbiswas.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;

public class Simplify {
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException("Precision Error");
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private static boolean isBalanced(String exp) {
        Stack<Character> st = new Stack<>();
        for (char it : exp.toCharArray()) {
            if (it == '(')
                st.push(it);
            if (it == ')') {
                if (st.isEmpty()) {
                    return false;
                } st.pop();
            }
        }
        if (st.isEmpty())
            return true;
        return false;
    }
    public static char validParentheses(String str){
        if(isBalanced(str) || str.charAt(str.length()-1)=='(')return '(';
        return ')';
    }

    public static String findErrorMessage(String str){
        boolean check = false;
        String temp = "";
        for(char it : str.toCharArray()){
            if(check == false){
                if(it == ' ')
                    check = true;
            }
            else if(check){
                temp += it;
            }
        }
        return temp;
    }



    public static String setBtnInput(String str, String btn){
        if(str.equals("0")){
            return btn;
        }
        return (str+btn);
    }
    public static boolean validDot(String str){
        for(int i=str.length()-1; i>=0; i--){
            if(Character.isDigit(str.charAt(i))==false){
                if(str.charAt(i)=='.')return false;
                return true;
            }
        }
        return true;
    }
}
