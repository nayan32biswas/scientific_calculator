package com.example.nayanbiswas.calculator;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Main {
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void main(String args[]){
        String str="(9)9";
        try{
            System.out.println("\n\n");
            Calculator C = new Calculator();
            System.out.println("Smart "+C.result(str));
        }
        catch (Exception e){
            System.out.println(Find(e.toString()));
        }
        double result = 999;
        str = String.format("%.2f", result);
        System.out.println(str);
        System.out.println(round(result, 6));
    }
    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException("Precision Error");
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    public static String Find(String str){
        boolean check = false;
        String temp = "";
        for(char it : str.toCharArray()){
            if(check == false){
                if(it == ' ')
                    check = true;
            }
            else if(check){
                temp += it;
            }
        }
        return temp;
    }
}

