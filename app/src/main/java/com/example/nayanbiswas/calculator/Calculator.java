package com.example.nayanbiswas.calculator;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.ArrayDeque;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    private static final String OPERATORS = "()s^!%/*+-";
    private static final int[] PRECEDENCE = {4, 4, 4, 3, 3, 3, 2, 2, 1, 1};
    private Stack<Character> operatorStack;
    private Stack<Double> operandStack;
    private ArrayDeque<String> postfixDeque;
    @RequiresApi(api = Build.VERSION_CODES.O)
    public double result(String input){
        input = clineUp(input);
        ArrayDeque ad = shunting(lex(input));
        //System.out.println("Postfix: " + ad);
        return evaluate(ad);
    }
    private String clineUp(String expre) {
        if (!isBalanced(expre)) {
            System.out.println("Not balance");
            throw new RuntimeException("...");
        }
        String temp = "(";
        try {
            for (char it : expre.toCharArray()) {
                if (it != ' ') {
                    if (temp.charAt(temp.length() - 1) == ')' && Character.isDigit(it)) {
                        temp += "*";
                    } else if (Character.isDigit(temp.charAt(temp.length() - 1)) && it == '(') {
                        temp += "*";
                    }
                    temp += it;
                }

            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return temp + ")";
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static ArrayDeque<Token> lex(String input) {
        ArrayDeque<Token> tokens = new ArrayDeque<>();
        String patterns = "(?<STARTNEGATE>^[-])|(?<OPERATOR>(?<![-*/+^!%])[-*/+^!%])|(?<LEFTPARENS>[(])|(?<RIGHTTPARENS>[)])|(?<SIN>sin\\(\\s*[-]?[0-9.\\s]+\\))|(?<NUMBER>[-]?[0-9.]+)|(?<WHITESPACE>[\t\f\r\n]+)";
        Pattern tokenPatterns = Pattern.compile(patterns);
        //System.out.println(tokenPatterns);
        try {
            Matcher matcher = tokenPatterns.matcher(input);
            while (matcher.find()) {
                if (matcher.group("NUMBER") != null) {
                    tokens.add(new Token("NUMBER", matcher.group("NUMBER")));
                    continue;
                } else if (matcher.group("OPERATOR") != null) {
                    tokens.add(new Token("OPERATOR", matcher.group("OPERATOR")));
                    continue;
                } else if (matcher.group("LEFTPARENS") != null) {
                    tokens.add(new Token("OPERATOR", matcher.group("LEFTPARENS")));
                    continue;
                } else if (matcher.group("RIGHTTPARENS") != null) {
                    tokens.add(new Token("OPERATOR", matcher.group("RIGHTTPARENS")));
                    continue;
                } else if (matcher.group("SIN") != null) {
                    tokens.add(new Token("SIN", matcher.group("SIN")));
                    continue;
                } else if (matcher.group("STARTNEGATE") != null) {
                    tokens.add(new Token("NUMBER", "-1"));
                    tokens.add(new Token("OPERATOR", "*"));
                    continue;
                } else if (matcher.group("WHITESPACE") != null)
                    continue;
                else if (matcher.group("DEGREE") != null) {
                    tokens.add(new Token("DEGREE", "cos"));
                }
            }
        }
        catch (Exception e){
            throw new RuntimeException("Lex Error");
        }
        //System.out.println(tokens);
        return tokens;
    }
    public ArrayDeque shunting(ArrayDeque<Token> infix) {
        operatorStack = new Stack<>();
        postfixDeque = new ArrayDeque<>();
        try {

            while (!infix.isEmpty()) {
                Token token = infix.poll();
                if (token.type == "OPERATOR") {
                    processOperator((token.data).charAt(0));
                } else if (token.type == "SIN") {
                    String d = "";
                    Pattern pattern = Pattern.compile("([-]?[0-9.]+)");
                    Matcher matcher = pattern.matcher(token.data);
                    while (matcher.find()) {
                        d = matcher.group(1);
                    }
                    processOperator('s');
                    postfixDeque.add(d);
                } else {
                    postfixDeque.add(token.data);
                }
            }
            while (!operatorStack.empty()) {
                char op = operatorStack.pop();
                postfixDeque.add(String.valueOf(op));
            }
        }
        catch (Exception e){
            throw new RuntimeException("Shunting Error");
        }
        return postfixDeque;
    }

    private void processOperator(char op) {
        try {
            if (operatorStack.empty()) {
                operatorStack.push(op);
            } else {
                char topOp = operatorStack.peek();
                if (op == '(') {
                    operatorStack.push(op);
                } else if ((topOp == '(' || precedence(op) > precedence(topOp)) && op != ')') {
                    operatorStack.push(op);
                } else if (op == ')') {
                    while (topOp != '(') {
                        operatorStack.pop();
                        postfixDeque.add(String.valueOf(topOp));
                        if (!operatorStack.empty()) {
                            topOp = operatorStack.peek();
                        }
                    }
                    operatorStack.pop();
                } else {
                    while (!operatorStack.empty() && precedence(op) <= precedence(topOp) && topOp != '(') {
                        operatorStack.pop();
                        postfixDeque.add(String.valueOf(topOp));
                        if (!operatorStack.empty()) {
                            topOp = operatorStack.peek();
                        }
                    }
                    operatorStack.push(op);
                }
            }
        }
        catch (Exception e){
            throw new RuntimeException("Process Operator");
        }
    }

    public double evaluate(ArrayDeque<String> postfix) {
        double result, first, second;
        System.out.println(postfix);
        try {
            operandStack = new Stack<>();
            while (!postfix.isEmpty()) {
                String c = postfix.poll();
                if (!isOperator(c.charAt(0)) || c.length() > 1) {
                    operandStack.push(Double.parseDouble(c));
                } else if (c.charAt(0) == '+') {
                    first = operandStack.pop();
                    second = operandStack.pop();
                    result = second + first;
                    operandStack.push(result);
                } else if (c.charAt(0) == '-' && c.length() == 1) {
                    first = operandStack.pop();
                    second = operandStack.pop();
                    result = second - first;
                    operandStack.push(result);
                } else if (c.charAt(0) == '/') {
                    first = operandStack.pop();
                    second = operandStack.pop();
                    result = second / first;
                    operandStack.push(result);
                } else if (c.charAt(0) == '*') {
                    first = operandStack.pop();
                    second = operandStack.pop();
                    result = first * second;
                    operandStack.push(result);
                } else if (c.charAt(0) == '^') {
                    first = operandStack.pop();
                    second = operandStack.pop();
                    result = Math.pow(second, first);
                    operandStack.push(result);
                } else if (c.charAt(0) == '!') {
                    first = operandStack.pop();
                    result = factorial(first);
                    operandStack.push(result);
                }else if (c.charAt(0) == '%') {
                    first = operandStack.pop();
                    result = (first/100);
                    operandStack.push(result);
                }
                else if (c.charAt(0) == 's') {
                    first = operandStack.pop();
                    result = Math.sin(first);
                    operandStack.push(result);
                }
            }

            return operandStack.pop();
        }
        catch (Exception e){
            throw new RuntimeException("Evaluate Error");
        }
    }

    private double factorial(double f) {
        // TODO: if f is of the form x.0, convert to int and do !
        if (f % 1 == 0) {
            int count = (int) f;
            while (count != 2) {
                count--;
                f *= count;
            }
            return f;
        }
        return la_gamma(f);
    }
    private double la_gamma(double x) {
        try {
            double[] p = {0.99999999999980993, 676.5203681218851, -1259.1392167224028, 771.32342877765313, -176.61502916214059, 12.507343278686905,-0.13857109526572012, 9.9843695780195716e-6, 1.5056327351493116e-7};
            int g = 7;
            if (x < 0.5) return Math.PI / (Math.sin(Math.PI * x) * la_gamma(1 - x));
            x -= 1;
            double a = p[0];
            double t = x + g + 0.5;
            for (int i = 1; i < p.length; i++) {
                a += p[i] / (x + i);
            }
            return Math.sqrt(2 * Math.PI) * Math.pow(t, x + 0.5) * Math.exp(-t) * a;
        }
        catch (Exception e){
            throw new RuntimeException("La_gamma Error");
        }
    }
    private boolean isOperator(char ch) {
        return OPERATORS.indexOf(ch) != -1;
    }
    private int precedence(char op){
        try {
            return PRECEDENCE[OPERATORS.indexOf(op)];
        }
        catch (Exception e){
            throw new RuntimeException("isOperator Error");
        }
    }
    private boolean isBalanced(String exp) {
        Stack<Character> st = new Stack<>();
        for (char it : exp.toCharArray()) {
            if (it == '(')
                st.push(it);
            if (it == ')') {
                if (st.isEmpty()) {
                    return false;
                } st.pop();
            }
        }
        if (st.isEmpty())
            return true;
        return false;
    }
    public static class Token {
        public String type;
        public String data;

        public Token(String type, String data) {
            this.type = type;
            this.data = data;
        }
        @Override
        public String toString() {
            return String.format("[%s : %s]", this.type, this.data);
        }
    }
}
